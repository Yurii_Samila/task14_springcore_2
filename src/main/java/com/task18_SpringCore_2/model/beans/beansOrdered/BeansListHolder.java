package com.task18_SpringCore_2.model.beans.beansOrdered;

import com.task18_SpringCore_2.service.BeanValidator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeansListHolder {

  @Autowired
  private List<BeanValidator> beans;

  public void printBeans(){
    for (BeanValidator bean : beans) {
      System.out.println(bean);
    }
  }

}
