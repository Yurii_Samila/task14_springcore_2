package com.task18_SpringCore_2.model.beans.beans2;

import org.springframework.stereotype.Component;

@Component
public class CatAnimal {

  private String furthColor;
  private int paw;

  public CatAnimal() {
  }

  public CatAnimal(String furthColor, int paw) {
    this.furthColor = furthColor;
    this.paw = paw;
  }

  public String getFurthColor() {
    return furthColor;
  }

  public void setFurthColor(String furthColor) {
    this.furthColor = furthColor;
  }

  public int getPaw() {
    return paw;
  }

  public void setPaw(int paw) {
    this.paw = paw;
  }

  @Override
  public String toString() {
    return "CatAnimal{" +
        "furthColor='" + furthColor + '\'' +
        ", paw=" + paw +
        '}';
  }
}
