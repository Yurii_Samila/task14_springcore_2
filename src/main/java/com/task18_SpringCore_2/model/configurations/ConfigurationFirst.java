package com.task18_SpringCore_2.model.configurations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = "com.task18_SpringCore_2.model.beans.beans1")
@ComponentScan(value = "com.task18_SpringCore_2.model.beans.beansOrdered")
@ComponentScan(basePackages = "com.task18_SpringCore_2.model.beans.otherBeans")
public class ConfigurationFirst {

}
