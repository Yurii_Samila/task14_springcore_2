package com.task18_SpringCore_2.model.beans.beansOrdered;

import com.task18_SpringCore_2.service.BeanValidator;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.LOWEST_PRECEDENCE - 2)
@Primary
public class BeanO2 implements BeanValidator {

  private double volume;

  @Override
  public void validate() {
  }

  @Override
  public String toString() {
    return "BeanO2{" +
        "volume=" + volume +
        '}';
  }
}
