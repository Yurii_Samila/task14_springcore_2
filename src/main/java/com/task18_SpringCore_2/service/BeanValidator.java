package com.task18_SpringCore_2.service;

public interface BeanValidator {
   void validate();
}
