package com.task18_SpringCore_2.model.beans.otherBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class OtherBeanField {

  @Autowired
  @Qualifier("otherBeanA")
  private OtherBeanA otherBeanA;
  @Autowired
  @Qualifier("customNameForOtherBeanB")
  private OtherBeanB otherBeanB;
  @Autowired
  private OtherBeanC otherBeanC;

  @Override
  public String toString() {
    return "OtherBeanField{" +
        "otherBeanA=" + otherBeanA +
        ", otherBeanB=" + otherBeanB +
        ", otherBeanC=" + otherBeanC +
        '}';
  }
}
