package com.task18_SpringCore_2.model.beans.beansProfile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class DevData extends BeanAbstract {
  public DevData(){
    name = "Nazar";
    value = 222;
  }

  @Override
  public String toString() {
    return "DevData{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }
}
