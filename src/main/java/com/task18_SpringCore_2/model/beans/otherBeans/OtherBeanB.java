package com.task18_SpringCore_2.model.beans.otherBeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("customNameForOtherBeanB")
@Scope("prototype")
public class OtherBeanB {

  private String description;
  private int value;


}
