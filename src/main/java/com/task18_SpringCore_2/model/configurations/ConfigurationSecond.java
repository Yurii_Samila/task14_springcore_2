package com.task18_SpringCore_2.model.configurations;

import com.task18_SpringCore_2.model.beans.beans3.BeanD;
import com.task18_SpringCore_2.model.beans.beans3.BeanF;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScans({
    @ComponentScan(value = "com.task18_SpringCore_2.model.beans.beans2",
    useDefaultFilters = false,
    includeFilters = @Filter(type = FilterType.REGEX, pattern = ".*Flower")),
    @ComponentScan(value = "com.task18_SpringCore_2.model.beans.beans3",
    useDefaultFilters = false,
    includeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE,classes = BeanD.class)),
    @ComponentScan(value = "com.task18_SpringCore_2.model.beans.beans3",
    useDefaultFilters = false,
    includeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE,classes = BeanF.class))
})
public class ConfigurationSecond {

}
