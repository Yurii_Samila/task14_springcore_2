package com.task18_SpringCore_2.model.beans.otherBeans;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanConstructor {

  private OtherBeanA otherBeanA;
  private OtherBeanB otherBeanB;
  private OtherBeanC otherBeanC;

  public OtherBeanConstructor(OtherBeanA otherBeanA,OtherBeanB otherBeanB, OtherBeanC otherBeanC){
    this.otherBeanA = otherBeanA;
    this.otherBeanB = otherBeanB;
    this.otherBeanC = otherBeanC;
  }

  @Override
  public String toString() {
    return "OtherBeanConstructor{" +
        "otherBeanA=" + otherBeanA +
        ", otherBeanB=" + otherBeanB +
        ", otherBeanC=" + otherBeanC +
        '}';
  }
}
