package com.task18_SpringCore_2.model.beans.otherBeans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanC {

  private String description;
  private int value;

}
