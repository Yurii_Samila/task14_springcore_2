package com.task18_SpringCore_2.model.beans.beansProfile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("test")
public class TestData extends BeanAbstract {
  public TestData(){
    name = "Oleg";
    value = 115;
  }

  @Override
  public String toString() {
    return "TestData{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }
}
