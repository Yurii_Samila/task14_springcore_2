package com.task18_SpringCore_2.model.beans.beansOrdered;

import com.task18_SpringCore_2.service.BeanValidator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.LOWEST_PRECEDENCE - 1)
@Qualifier("lastBeanO")
public class BeanO1 implements BeanValidator {

  private double volume;

  @Override
  public void validate() {
  }

  @Override
  public String toString() {
    return "BeanO1{" +
        "volume=" + volume +
        '}';
  }
}
