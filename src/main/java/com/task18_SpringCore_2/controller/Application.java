package com.task18_SpringCore_2.controller;

import com.task18_SpringCore_2.model.beans.beansOrdered.BeansHolder;
import com.task18_SpringCore_2.model.beans.beansOrdered.BeansListHolder;
import com.task18_SpringCore_2.model.beans.beansProfile.BeanOne;
import com.task18_SpringCore_2.model.beans.beansProfile.BeanTwo;
import com.task18_SpringCore_2.model.beans.otherBeans.OtherBeanA;
import com.task18_SpringCore_2.model.beans.otherBeans.OtherBeanB;
import com.task18_SpringCore_2.model.beans.otherBeans.OtherBeanC;
import com.task18_SpringCore_2.model.configurations.ConfigurationFirst;
import com.task18_SpringCore_2.model.configurations.ConfigurationProfileBean;
import com.task18_SpringCore_2.model.configurations.ConfigurationProfileComponent;
import com.task18_SpringCore_2.model.configurations.ConfigurationSecond;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

public class Application {

  public static void main(String[] args) {
    List<Object> list = new ArrayList<>();
    ApplicationContext context = new AnnotationConfigApplicationContext(ConfigurationFirst.class,
        ConfigurationSecond.class);
//    list.add(context.getBean(BeanA.class));
//    list.add(context.getBean(BeanB.class));
//    list.add(context.getBean(BeanC.class));
//    list.add(context.getBean(BeanD.class));
//    list.add(context.getBean(BeanE.class));
//    list.add(context.getBean(BeanF.class));
//    list.add(context.getBean(CatAnimal.class));
//    list.add(context.getBean(NarcissusFlower.class));
//    list.add(context.getBean(RoseFlower.class));
//    context.getBean(BeansListHolder.class).printBeans();
//    System.out.println(context.getBean(BeansHolder.class));
//    System.out.println(context.getBean(OtherBeanA.class));
//    System.out.println(context.getBean(OtherBeanA.class));
//    System.out.println(context.getBean(OtherBeanB.class));
//    System.out.println(context.getBean(OtherBeanB.class));
//    System.out.println(context.getBean(OtherBeanC.class));
//    System.out.println(context.getBean(OtherBeanC.class));
    AnnotationConfigApplicationContext context1 = new AnnotationConfigApplicationContext();
    context1.getEnvironment().setActiveProfiles("test");
    context1.register(ConfigurationProfileComponent.class);
    context1.refresh();
    System.out.println(context1.getBean(BeanTwo.class));
    System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "dev");
    AnnotationConfigApplicationContext context2 = new AnnotationConfigApplicationContext(ConfigurationProfileComponent.class);
    System.out.println(context2.getBean(BeanTwo.class));



  }
}
