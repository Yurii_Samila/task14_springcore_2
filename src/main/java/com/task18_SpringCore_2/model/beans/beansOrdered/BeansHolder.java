package com.task18_SpringCore_2.model.beans.beansOrdered;

import com.task18_SpringCore_2.service.BeanValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeansHolder {

  @Autowired
  BeanValidator bean1;
  @Autowired
  @Qualifier("beanO3")
  BeanValidator bean2;
  @Autowired
  @Qualifier("lastBeanO")
  BeanValidator bean3;

  @Override
  public String toString() {
    return "BeansHolder{" +
        "bean1=" + bean1 +
        ", bean2=" + bean2 +
        ", bean3=" + bean3 +
        '}';
  }
}
