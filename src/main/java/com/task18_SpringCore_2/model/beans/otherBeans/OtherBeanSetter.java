package com.task18_SpringCore_2.model.beans.otherBeans;

import com.task18_SpringCore_2.model.beans.beans1.BeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OtherBeanSetter {

  private OtherBeanA otherBeanA;
  private OtherBeanB otherBeanB;
  private OtherBeanC otherBeanC;

  public OtherBeanSetter() {
  }

  @Autowired
  public void setOtherBeanA(OtherBeanA otherBeanA){
    this.otherBeanA = otherBeanA;
  }

  @Autowired
  public void setOtherBeanB(OtherBeanB otherBeanB){
    this.otherBeanB = otherBeanB;
  }

  @Autowired
  public void setOtherBeanC(OtherBeanC otherBeanC){
    this.otherBeanC = otherBeanC;
  }

  @Override
  public String toString() {
    return "OtherBeanSetter{" +
        "otherBeanA=" + otherBeanA +
        ", otherBeanB=" + otherBeanB +
        ", otherBeanC=" + otherBeanC +
        '}';
  }
}
