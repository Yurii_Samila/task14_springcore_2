package com.task18_SpringCore_2.model.configurations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.task18_SpringCore_2.model.beans.beansProfile")
public class ConfigurationProfileComponent { }
