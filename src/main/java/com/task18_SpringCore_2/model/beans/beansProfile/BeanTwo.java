package com.task18_SpringCore_2.model.beans.beansProfile;

import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanTwo {

  @Autowired
  private BeanAbstract beanAbstract;

  @Override
  public String toString() {
    return beanAbstract.toString();
  }
}
