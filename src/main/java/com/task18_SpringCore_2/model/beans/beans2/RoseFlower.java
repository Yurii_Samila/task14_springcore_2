package com.task18_SpringCore_2.model.beans.beans2;

import org.springframework.stereotype.Component;

@Component
public class RoseFlower {

  private String color;
  private int letters;

  public RoseFlower() {
  }

  public RoseFlower(String color, int letters) {
    this.color = color;
    this.letters = letters;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public int getLetters() {
    return letters;
  }

  public void setLetters(int letters) {
    this.letters = letters;
  }

  @Override
  public String toString() {
    return "RoseFlower{" +
        "color='" + color + '\'' +
        ", letters=" + letters +
        '}';
  }
}
