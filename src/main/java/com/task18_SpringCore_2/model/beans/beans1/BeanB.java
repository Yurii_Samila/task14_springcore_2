package com.task18_SpringCore_2.model.beans.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanB {
  private String name;
  private int age;

  public BeanB() {
  }

  public BeanB(String name, int age) {
    this.name = name;
    this.age = age;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  @Override
  public String toString() {
    return "BeanB{" +
        "name='" + name + '\'' +
        ", age=" + age +
        '}';
  }
}
