package com.task18_SpringCore_2.model.configurations;

import com.task18_SpringCore_2.model.beans.beansProfile.BeanOne;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ConfigurationProfileBean {

  @Bean
  @Profile("test")
  public BeanOne getBeanOneTest(){
    return new BeanOne("Bob",25);
  }
  @Bean
  @Profile("dev")
  public BeanOne getBeanOneDev(){
    return new BeanOne("John",50);
  }
}
